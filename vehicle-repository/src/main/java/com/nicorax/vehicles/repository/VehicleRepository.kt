package com.nicorax.vehicles.repository

import android.location.Location
import com.nicorax.vehicle.persistence.VehicleData
import com.nicorax.vehicles.repository.data.VehicleDetail
import io.reactivex.Single
import io.reactivex.rxjava3.core.Observable
import io.reactivex.rxjava3.subjects.PublishSubject

interface VehicleRepository {

    val onRefreshVehicleAdd: PublishSubject<VehicleDetail>

    fun getVehicles(isActive: Boolean): Observable<VehicleDetail>

    fun isLessDistance(locationReference: Location, locationVehicle: Location, distanceKm: Double): Boolean

    fun getVehicles(): Single<List<VehicleDetail>>

    fun getVehicle(vehicleId: String): Single<VehicleDetail>

    fun saveVehicle(vehicleIdentifier: String, latitude: Double, longitude: Double, isNear: Boolean)

    fun getLocationInglewood(): Location
}