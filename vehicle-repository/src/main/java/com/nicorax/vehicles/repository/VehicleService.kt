package com.nicorax.vehicles.repository

import com.nicorax.vehicles.repository.data.Vehicles
import com.nicorax.vehicles.repository.data.VehicleDetail
import io.reactivex.rxjava3.core.Observable
import retrofit2.http.GET
import retrofit2.http.Path

interface VehicleService {

    @GET("vehicles")
    fun getVehicles(): Observable<List<Vehicles>>

    @GET("vehicles/{id}")
    fun getVehicle(@Path("id") id: String): Observable<VehicleDetail>
}