package com.nicorax.vehicles.repository.data

data class Vehicles(val vehicleId: String?, val isActive: Boolean?)
