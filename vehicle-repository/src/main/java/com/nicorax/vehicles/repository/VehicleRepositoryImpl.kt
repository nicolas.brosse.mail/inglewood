package com.nicorax.vehicles.repository

import android.annotation.SuppressLint
import android.content.Context
import android.location.Location
import com.nicorax.vehicle.persistence.VehicleData
import com.nicorax.vehicle.persistence.VehiclesDataBase
import com.nicorax.vehicles.repository.data.VehicleDetail
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.rxjava3.core.Flowable.fromIterable
import io.reactivex.rxjava3.core.Observable
import io.reactivex.rxjava3.core.Observable.fromIterable
import io.reactivex.rxjava3.subjects.PublishSubject
import io.reactivex.schedulers.Schedulers

class VehicleRepositoryImpl(private val vehicleService: VehicleService, private val context: Context) : VehicleRepository {

    override val onRefreshVehicleAdd: PublishSubject<VehicleDetail> = PublishSubject.create()

    override fun getVehicles(isActive: Boolean): Observable<VehicleDetail> =
        vehicleService.getVehicles()
            .flatMap { Observable.fromIterable(it) }
            .filter { isActive == it.isActive }
            .flatMap {
                val id = checkNotNull(it.vehicleId) { "vehicleId is null" }
                vehicleService.getVehicle(id).observeOn(io.reactivex.rxjava3.schedulers.Schedulers.io())
            }

    override fun isLessDistance(
        locationReference: Location,
        locationVehicle: Location,
        distanceKm: Double
    ): Boolean {
        val km = locationReference.distanceTo(locationVehicle) / 1000
        return km < distanceKm
    }

    override fun getVehicles(): Single<List<VehicleDetail>> = VehiclesDataBase.getInstance(context).VehicleDao().getVehicles().flatMap {
        val list = it.map { vehicleData ->
            convertVehicleDataToVehicleDetail(vehicleData)
        }
        Single.just(list)
    }

    override fun getVehicle(vehicleId: String): Single<VehicleDetail> = VehiclesDataBase.getInstance(context).VehicleDao().getVehicle(vehicleId).flatMap {
        Single.just(it.firstOrNull()?.let { vehicleData ->
            convertVehicleDataToVehicleDetail(vehicleData)
        })
    }

    @SuppressLint("CheckResult")
    override fun saveVehicle(vehicleIdentifier: String, latitude: Double, longitude: Double, isNear: Boolean) {
        VehiclesDataBase.getInstance(context).VehicleDao().insertVehicle(
            VehicleData(
                vehicleIdentifier = vehicleIdentifier,
                latitude = latitude,
                longitude = longitude
            )
        ).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe({
            println("ici ici ok ok")
            onRefreshVehicleAdd.onNext(VehicleDetail(vehicleId = vehicleIdentifier, com.nicorax.vehicles.repository.data.Location(latitude, longitude), isNear))
        }, {
            println("ici ici not ok $it")
        })
    }

    override fun getLocationInglewood(): Location = Location("Inglewood").apply {
        longitude = 6.6314437
        latitude = 46.5223916
    }

    private fun convertVehicleDataToVehicleDetail(vehicleData: VehicleData) = VehicleDetail(
        vehicleId = vehicleData.vehicleIdentifier,
        com.nicorax.vehicles.repository.data.Location(latitude = vehicleData.latitude, longitude = vehicleData.longitude),
        isNear = vehicleData.isNear
    )
}