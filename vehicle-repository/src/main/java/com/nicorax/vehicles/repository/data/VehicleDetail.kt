package com.nicorax.vehicles.repository.data

data class VehicleDetail(val vehicleId: String?, val location: Location?, val isNear: Boolean)

data class Location(val latitude: Double?, val longitude: Double?)