package com.nicorax.home.ui

import android.annotation.SuppressLint
import android.location.Location
import androidx.lifecycle.ViewModel
import com.nicorax.vehicles.repository.VehicleRepository
import com.nicorax.vehicles.repository.data.VehicleDetail
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.rxjava3.core.Observable
import io.reactivex.rxjava3.subjects.PublishSubject
import io.reactivex.schedulers.Schedulers

sealed class VehicleState {
    data class VehicleMovement(val vehicle: VehicleUi) : VehicleState()
    data class VehicleUi(val id: String, val latitude: Double, val longitude: Double, val isNear: Boolean)
    object Error : VehicleState()
}

class HomeViewModel(private val vehicleRepository: VehicleRepository) : ViewModel() {

    private val _actionState: PublishSubject<VehicleState> = PublishSubject.create()
    val actionState: Observable<VehicleState> = _actionState.hide()

    private var disposable : Disposable? = null

    init {
        vehicleRepository.onRefreshVehicleAdd.subscribe {
            _actionState.onNext(VehicleState.VehicleMovement(convertVehicleDetailToVehicleUi(it)))
        }
    }

    @SuppressLint("CheckResult")
    fun onResume(){
        disposable = vehicleRepository.getVehicles()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                it.forEach {
                    _actionState.onNext(VehicleState.VehicleMovement(convertVehicleDetailToVehicleUi(it)))
                }
            }, {

            })
    }

    fun onPause(){
        disposable?.dispose()
    }

    private fun convertVehicleDetailToVehicleUi(vehiclesDetail: VehicleDetail): VehicleState.VehicleUi {
        val id = checkNotNull(vehiclesDetail.vehicleId) { "vehiclesDetail.vehicleId is null" }
        val latitude = checkNotNull(vehiclesDetail.location?.latitude) { "vehiclesDetail.vehicleId is null" }
        val longitude = checkNotNull(vehiclesDetail.location?.longitude) { "vehiclesDetail.location.longitude" }

        return VehicleState.VehicleUi(
            id = id,
            latitude = latitude,
            longitude = longitude,
            isNear = vehiclesDetail.isNear
        )
    }
}