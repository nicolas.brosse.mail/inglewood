package com.nicorax.home.ui.di

import com.nicorax.home.ui.HomeViewModel
import com.nicorax.vehicles.repository.BuildConfig
import com.nicorax.vehicles.repository.VehicleRepository
import com.nicorax.vehicles.repository.VehicleRepositoryImpl
import com.nicorax.vehicles.repository.VehicleService
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import org.koin.android.ext.koin.androidContext
import org.koin.core.parameter.parametersOf
import org.koin.dsl.module
import retrofit2.Retrofit
import retrofit2.adapter.rxjava3.RxJava3CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory

val vehicleRepositoryModule = module {

    single { HomeViewModel(get()) }

    single<VehicleRepository> { VehicleRepositoryImpl(get(), androidContext()) }

    single {
        HttpLoggingInterceptor().apply {
            level = if (BuildConfig.DEBUG) {
                HttpLoggingInterceptor.Level.BODY
            } else {
                HttpLoggingInterceptor.Level.NONE
            }
        }
    }

    factory {
        OkHttpClient.Builder().apply {
            addInterceptor(Interceptor { chain ->
                val builder = chain.request().newBuilder()
                builder.header("X-Platform", "Android")
                builder.header("x-api-key", com.nicorax.home.ui.BuildConfig.API_KEY)
                return@Interceptor chain.proceed(builder.build())
            })
            addInterceptor(get<HttpLoggingInterceptor>())
        }
    }


    factory<Retrofit> { (baseUrl: String, okHttpClient: OkHttpClient.Builder) ->
        Retrofit.Builder()
            .addConverterFactory(GsonConverterFactory.create())
            .addCallAdapterFactory(RxJava3CallAdapterFactory.create())
            .baseUrl(baseUrl)
            .client(okHttpClient.build())
            .build()
    }

    single<VehicleService> {
        val okClientBuilder = get<OkHttpClient.Builder>()
        get<Retrofit> {
            parametersOf(
                com.nicorax.home.ui.BuildConfig.API_BASE_URL,
                okClientBuilder
            )
        }.create(
            VehicleService::class.java
        )
    }
}