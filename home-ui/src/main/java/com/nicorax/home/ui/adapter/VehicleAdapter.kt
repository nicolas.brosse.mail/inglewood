package com.nicorax.home.ui.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.annotation.NonNull
import androidx.recyclerview.widget.RecyclerView
import com.nicorax.home.ui.R
import com.nicorax.home.ui.VehicleState

internal class VehicleAdapter(private val itemsList: MutableList<VehicleState.VehicleUi> = emptyList<VehicleState.VehicleUi>().toMutableList()) :
    RecyclerView.Adapter<VehicleAdapter.VehicleViewHolder>() {

    @NonNull
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): VehicleViewHolder {
        val itemView = LayoutInflater.from(parent.context).inflate(R.layout.list_item_vehicle, parent, false)
        return VehicleViewHolder(itemView)
    }

    override fun onBindViewHolder(holder: VehicleViewHolder, position: Int) {
        holder.onBind(itemsList[position])
    }

    override fun getItemCount(): Int {
        return itemsList.size
    }

    fun addItem(item: VehicleState.VehicleUi) {
        itemsList.add(0, item)
        notifyDataSetChanged()
    }

    internal inner class VehicleViewHolder(private val view: View) : RecyclerView.ViewHolder(view) {
        private val tvId: TextView = view.findViewById(R.id.tv_id)
        private val tvLatitude: TextView = view.findViewById(R.id.tv_latitude)
        private val tvLongitude: TextView = view.findViewById(R.id.tv_longitude)
        private val tvNear: TextView = view.findViewById(R.id.tv_is_near)

        fun onBind(vehicle: VehicleState.VehicleUi) {
            tvId.text = view.context.getString(R.string.home_identifier, vehicle.id)
            tvLatitude.text = view.context.getString(R.string.home_latitude, vehicle.latitude)
            tvLongitude.text = view.context.getString(R.string.home_longitude, vehicle.longitude)
            tvNear.text =
                if (vehicle.isNear) view.context.getString(R.string.home_vehicle_near) else view.context.getString(R.string.home_vehicle_vehicles_moving_away)
        }
    }
}