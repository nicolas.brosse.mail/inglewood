package com.nicorax.home.ui.service

import android.annotation.SuppressLint
import android.app.Service
import android.content.Intent
import android.location.Location
import android.os.IBinder
import android.util.Log
import android.widget.Toast
import androidx.core.app.NotificationCompat
import androidx.core.app.NotificationManagerCompat
import com.nicorax.home.ui.HomeViewModel
import com.nicorax.home.ui.R
import com.nicorax.vehicles.repository.VehicleRepository
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers
import io.reactivex.rxjava3.core.Observable
import io.reactivex.rxjava3.schedulers.Schedulers
import org.koin.android.ext.android.inject
import java.util.*
import java.util.concurrent.TimeUnit

class VehicleStateService : Service() {

    private val vehicleRepository: VehicleRepository by inject()

    private val locationDefault = vehicleRepository.getLocationInglewood()

    private var disposable : io.reactivex.rxjava3.disposables.Disposable? = null

    override fun onStartCommand(intent: Intent, flags: Int, startId: Int): Int {
        return START_STICKY
    }

    @SuppressLint("CheckResult")
    override fun onCreate() {
        super.onCreate()

        disposable = Observable.interval(2, TimeUnit.SECONDS).subscribeOn(Schedulers.io())
            .flatMap {
                vehicleRepository.getVehicles(isActive = true).retry(3)
            }.observeOn(AndroidSchedulers.mainThread())
            .subscribe({ vehicleDetail ->
                val vehicleId = checkNotNull(vehicleDetail.vehicleId) { "vehicleId is null" }
                val latitude = checkNotNull(vehicleDetail.location?.latitude) { "location.latitude is null" }
                val longitude = checkNotNull(vehicleDetail.location?.longitude) { "location.longitude is null" }

                val vehicleLocation = Location(vehicleId)
                vehicleLocation.latitude = latitude
                vehicleLocation.longitude = longitude

                //Vehicle in the area 1km
                if (vehicleRepository.isLessDistance(locationDefault, vehicleLocation, 1.0)) {
                    vehicleRepository.saveVehicle(vehicleIdentifier = vehicleId, latitude, longitude, true)
                    showNotification(vehicleId)
                } else {
                    vehicleRepository.getVehicle(vehicleId)
                        .subscribeOn(io.reactivex.schedulers.Schedulers.io()).observeOn(io.reactivex.android.schedulers.AndroidSchedulers.mainThread())
                        .subscribe({
                            if (it.isNear) {
                                vehicleRepository.saveVehicle(vehicleIdentifier = vehicleId, latitude, longitude, false)
                                Toast.makeText(
                                    applicationContext,
                                    applicationContext.getString(R.string.home_vehicle_vehicles_moving_away_id, vehicleId),
                                    Toast.LENGTH_SHORT
                                ).show()
                            }
                        }, {
                            Log.d(HomeViewModel::class.qualifiedName, "An error occured", it)
                        })
                }
            }, {
                Log.d(HomeViewModel::class.qualifiedName, "An error occured", it)
            })
    }

    override fun onBind(intent: Intent): IBinder? {
        return null
    }

    override fun onDestroy() {
        disposable?.dispose()
        super.onDestroy()
    }

    private fun showNotification(idVehicle: String) {
        val notificationBuilder: NotificationCompat.Builder =
            NotificationCompat.Builder(applicationContext)
                .setSmallIcon(com.google.android.material.R.drawable.ic_mtrl_chip_checked_circle)
                .setContentTitle(applicationContext.getString(R.string.home_vehicle_in_the_area))
                .setContentText(applicationContext.getString(R.string.home_identifier, idVehicle))

        val notificationManager = NotificationManagerCompat.from(applicationContext)

        notificationManager.notify((Date().time / 100).toInt(), notificationBuilder.build())
    }
}