package com.nicorax.home.ui

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.LinearLayout
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.nicorax.home.ui.adapter.VehicleAdapter
import com.nicorax.home.ui.service.VehicleStateService
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers
import org.koin.android.ext.android.inject


class HomeFragment : Fragment(R.layout.fragment_home) {

    private val viewModel: HomeViewModel by inject()

    private lateinit var customAdapter: VehicleAdapter

    private var llEmpty: LinearLayout? = null

    private var disposable : io.reactivex.rxjava3.disposables.Disposable? = null

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        llEmpty = view.findViewById(R.id.ll_empty_state)

        activity?.also {
            customAdapter = VehicleAdapter()

            it.findViewById<RecyclerView>(R.id.recyclerView)?.apply {
                layoutManager = LinearLayoutManager(it.applicationContext)
                adapter = customAdapter
            }
        }

        activity?.startService(Intent(activity?.applicationContext, VehicleStateService::class.java))
    }

    override fun onResume() {
        super.onResume()
        viewModel.onResume()
        viewModel.actionState.observeOn(AndroidSchedulers.mainThread()).subscribe {
            Log.d(HomeFragment::class.qualifiedName, "[Action] Receive $it")
            when (it) {
                is VehicleState.VehicleMovement -> {
                    llEmpty?.visibility = View.GONE
                    customAdapter.addItem(it.vehicle)
                }
                is VehicleState.Error -> {
                    Toast.makeText(requireContext(), getString(R.string.on_error_detected), Toast.LENGTH_SHORT).show()
                }
            }
        }
    }

    override fun onPause() {
        viewModel.onPause()
        disposable?.dispose()
        super.onPause()
    }
}