package com.nicorax.vehicle.persistence

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = VehicleData.TABLE_NAME)
data class VehicleData(
    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = ID)
    var vehicleID: Int? = null,
    @ColumnInfo(name = VEHICLE_IDENTIFIER)
    var vehicleIdentifier: String,
    @ColumnInfo(name = LATITUDE)
    var latitude: Double,
    @ColumnInfo(name = LONGITUDE)
    var longitude: Double,
    @ColumnInfo(name = IS_NEAR)
    var isNear: Boolean = true
) {
    companion object {
        const val TABLE_NAME = "vehicle"
        const val ID = "id"
        const val VEHICLE_IDENTIFIER = "vehicle_identifier"
        const val LATITUDE = "latitude"
        const val LONGITUDE = "longitude"
        const val IS_NEAR = "is_near"
    }
}