package com.nicorax.vehicle.persistence

import androidx.room.*
import io.reactivex.Completable
import io.reactivex.Single


@Dao
interface VehicleDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertVehicle(vehicle: VehicleData): Completable

    @Query("SELECT * FROM ${VehicleData.TABLE_NAME} ORDER BY ${VehicleData.ID} DESC")
    fun getVehicles(): Single<List<VehicleData>>

    @Query("SELECT * FROM ${VehicleData.TABLE_NAME} WHERE ${VehicleData.VEHICLE_IDENTIFIER} = :vehicleId ORDER BY ${VehicleData.ID} DESC")
    fun getVehicle(vehicleId : String): Single<List<VehicleData>>

}