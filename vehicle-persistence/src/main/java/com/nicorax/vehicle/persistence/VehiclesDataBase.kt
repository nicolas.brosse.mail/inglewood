package com.nicorax.vehicle.persistence

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase

@Database(entities = [VehicleData::class], version = DB_VERSION)
abstract class VehiclesDataBase : RoomDatabase() {

    abstract fun VehicleDao(): VehicleDao

    companion object {
        @Volatile
        private var databaseInstance: VehiclesDataBase? = null

        fun getInstance(context: Context): VehiclesDataBase =
            databaseInstance ?: synchronized(this) {
                databaseInstance ?: buildDatabaseInstance(context).also {
                    databaseInstance = it
                }
            }

        private fun buildDatabaseInstance(context: Context) =
            Room.databaseBuilder(context, VehiclesDataBase::class.java, DB_NAME).fallbackToDestructiveMigration().build()

    }
}

const val DB_VERSION = 2

const val DB_NAME = "VehicleData.db"