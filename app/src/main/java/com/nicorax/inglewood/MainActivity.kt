package com.nicorax.inglewood

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.nicorax.home.ui.HomeFragment


class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        if (savedInstanceState == null) {
            supportFragmentManager.beginTransaction().add(R.id.fragment_main, HomeFragment()).commit()
        }
    }
}